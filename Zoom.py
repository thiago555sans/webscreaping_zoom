import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from datetime import datetime
from selenium.webdriver.chrome.options import Options
import json
import time

produto="a21s"
url = "https://www.zoom.com.br/search?q="+produto 
path = 'C:/Users/thiago.santos/Documents/Codes/Python/Webscreaping/Webdrivers/chromedriver.exe'

#Funçaõ que recebe um elemento html e retorna o texto do elemento quando existe
def get_text(element):
    retorno=[]
    try:
        loja= element.find('span',attrs={"class":"MerchantBrand_BrandName__2u7pT"}).text
    except AttributeError:
        loja='Null'

    try:
        preco=element.find('a',attrs={"class":"PriceBox_Value__2VuFN"}).text
    except AttributeError:
        preco='Null'

    try:
        pgt_detalhe = element.find('span',attrs={"class":"PaymentDetails_FirstLabel__3bxaA"}).text
    except AttributeError:
        pgt_detalhe='Null'
    
    retorno.append(loja)
    retorno.append(preco)
    retorno.append(pgt_detalhe)
    return retorno


#Instanciar o browser
option = Options()
option.headless = True
driver = webdriver.Chrome(executable_path=path)
driver.set_window_size(1000, 1000)
driver.get(url)

#time.sleep(3)
driver.find_element_by_xpath('//*[@id="resultArea"]/div[3]/div/div[1]/div[2]/div[3]/a').click()

#verifica se existem um botão que mostra mais ofertas, se houver ele seleciona para listar todas as ofertas 
time.sleep(1)
try:
    driver.find_element_by_xpath('//*[@id="__next"]/div[1]/div[3]/div[2]/div/div/section[4]/button').click()
except:
    pass

#seleciona a lista com os valores do produto
element = driver.find_element_by_xpath('//*[@id="__next"]/div[1]/div[3]/div[2]/div/div/section[4]/div[2]/div[2]/div/ul')


html_content = element.get_attribute('outerHTML')
soup = BeautifulSoup(html_content,'html.parser')
lista = soup.findAll(name='li')


#Abre o arquivo
fp = open('produto_26.csv','a')
#fp.write('produto,loja,valor,parcelas,dia\n')
#percorre Li da Ul e retorna o texto
for l in lista:
    fields = get_text(l)
    if (fields[0] != 'Null'):
        fp.write(produto+','+fields[0]+','+str(fields[1].replace('R$ ','').replace('.','').replace(',','.'))+','+fields[2].replace(',','.')+','+str(datetime.now().day)+'\n')
#Fecha o arquivo
fp.close 
#Encerra browser
driver.quit()



